[![pipeline status](https://gitlab.com/neodim5/red-five-gb-wp/badges/main/pipeline.svg)](https://gitlab.com/neodim5/red-five-gb-wp/-/commits/main)
[![Latest Release](https://gitlab.com/neodim5/red-five-gb-wp/-/badges/release.svg)](https://gitlab.com/neodim5/red-five-gb-wp/-/releases)

# Red Five GB WP

Командный проект GB
Развертывание инфраструктуры на VK Cloud (MCS) с установкой WordPress MySQL Prometheus NGINX-LB

## О проекте (Description)

Этот проект сосздан как экзаменационная работа студетов факультета DevOps GeekBrains. Командный проект группы №6 RedTeam.

## Техническое задание
1. Операционная система виртуальных машин Ubuntu 20.04LTS
2. Ресурсные емкости всех виртуальных машин 1vcpu, 2G vram, 20Gb диск.
3. Система управления конфигурациями Ansible
4. Версии БД - последние stable, совместимые с актуальной версией WordPress
Инфраструктура доступна облачная инфраструтура VK Cloud. 

Состоящая из:
- сервер баз данных mysql
- два сервера приложений
- сервер мониторинга

Высокой доступности серверов приложений для пользователя планируется достигать:
- применением round-robin DNS
- “перекрестным опылением” на реверс-прокси (при недоступности локального
апстрима, запрос должен уходить на второй сервер)

Консистентности данных на серверах приложений планируется достигать
двусторонней синхронизацией набора файлов на серверах app01 - 02.

## Установка проекта (Installation)
Весь проект построен на Ansible и openstack-client.
Для установки всех необходимых зависимостей создано несколько скриптов для OS Ubuntu 20.04 (Debian family).

```
./script/requirements.sh - установка всех зависимойтей для проекта.
./START.sh - установка всех зависимостей и запуска развертывания инфраструктуры согласно задания.
```
Для устпшной отработки скриптов ansible необходимо авторизаваться в вашем проекте в облачном сервисе VK Cloud (бывший MCS).
Для этого необходимо скачать файл авторизации из вашего проета (ваш аккаунт (вверхний правый угол) / Настройки проекта / API ключи) в папку ./key (файл должен быть формата mcs12345678-openrc.sh). 

```
./key/mcs12345678-openrc.sh
```

Также необхожимо сгенерировать свою новую ключевую пару для доступа к создаваемым инстансам в облаке. Необхомимо разместить публичный ключ в ./key/ansible/id_rsa.pub. Этот ключ будет использоваться при создании нового инстанса как ключевая пара "ansible-key".

```
./key/ansible/id_rsa.pub
```

# Переменные инфраструктуры

Изменение переменных инфраструтуры осуществляются через
```
./vars/iac_vars.yml
```
в этом файле описна вся инфраструктура (инстансы, зона доступности, имена групп безопасности, имена сетей, подсетей и имена инстансов и их параметры)

Для организации доступа других пользователей и их ssh ключей необходимо скопировать или создать файл ./key/authorized_keys и в него добавить дополнительный ключи для организации доступа к серверам.

```
./key/authorized_keys
```

Используемый пользователь по умолчанию ubuntu. Изменить его пароль можно через групповые переменные ./group_vars/all 

```
newpassword: "!qawsedrftg123456"
```

Перед запуском необходимо проверить ваши переменные для инстансов. Например dns имя вашего проекта, имя пользователя и пароли для доступов к сервисам. 

```
./group_vars/app
http_host: "wp2.neodim5.ru"
```
```
./group_vars/lb
virtual_domain: "wp2.neodim5.ru"
domain_name: wp2.neodim5.ru
```

для настройки доступа к базе данных для приложения WordPress
```
./group_vars/app
# WP settings for connection to db
mysql_db: "wordpress" - имя базы данных
mysql_user: "sully" - пользователь для базы данных
mysql_password: "sully" - пароль пользователя бля базы данных
```

настройка базы данных должны совпадать с настройками приложения
```
./group_vars/database
mysql_root_password: "root" 
mysql_db: "wordpress"
mysql_user: "sully"
mysql_password: "sully"
```

## Предоставления доступа и ключи

Для организации доступа к облачному сервису MCS необходимо поместить в папку ./key ваши ключи доступа к облаку (./key/mcs1354673.sh), а также ssh ключи для размещения на серверах инфраструктуры.
Нужно перезапуском развертывания применить
```
source ./key/mcs12345678.sh
запросит у вас пароль от вашнй учетной записи проекта VK Cloud
env | grep -i os
```

## Запуск проекта (Usage)

Для запуска проекта достаточно дать права на запуск скрипту 
```
chmod +x START.sh
```
проверить нахождение файлов ключей авторизации на местах (см Переменные инфраструктуры и Установка)

изапустиь файл скрипт от имени пользователя имеющий права sudo
```
START.sh
```
Будут установлены все необходимые зависимости и после запросит пароль от облака VK Cloud и запустится установка инфрпаструтуры в облаке, а после установка приложенийи их конфигурация согласно задания.

## CI CD
If you have ideas for releases in the future, it is a good idea to list them in the README.


## Authors and acknowledgment
Неклюдов Дмитрий
Ермолович Александр
Кириллов Сергей

## License
GPL

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

