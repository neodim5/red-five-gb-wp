#!/bin/bash
echo Устаовка всех зависимостей для работы скриптов и нашей роли развертывния Инфраструктуры

echo установка клиента для openstack и pip3
sudo apt install python3-openstackclient python3-pip software-properties-common

echo установка ansible и пакета зависимоткей солекции openstack.cloud и изменения паролья пользователя
pip3 install ansible openstacksdk passlib

echo установка колекции ansible для проекта
ansible-galaxy collection install openstack.cloud
ansible-galaxy collection install community.mysql
