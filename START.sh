#!/bin/bash

echo запуск скрипта установки всех зависимостей
./scripts/requirements.sh

key_mcs=./key/mcs[0-9]*-openrc.sh

echo Проверим наличие файла авторизации для облака

ls -l $key_mcs 2> /dev/null
if [[ $? -eq 0 ]]
then
  echo "Файл авторизации в облако на месте"
else
  echo "Необходимо скачать файл для авторизации в облаке VK Cloud и положить его в директорию ./key" >&2
  exit 1
fi


echo Необходимо ввсети пароль от проекта в MCS
source $key_mcs

ansible-playbook main.yml
